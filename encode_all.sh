#! /bin/sh
# readlink isn't a standard tool unfortunately
# this just makes sure csv2ascii is in the PATH, though. Edit as necessary.
PATH="$PATH"':'"$(dirname "$(readlink -f "$0")")"
OLDPWD="$PWD"
cd editing
for file in FS*Save; do
  # ../file might behave weirdly on some systems in my experience if there are
  # any symlinks involved. This OLDPWD trick probably works more reliably.
  ascii2csv "$file" > "$OLDPWD"'/'"$file"
done
