#include <stdio.h>
#include <stdlib.h>

void strdec(int achar, FILE *infile)
{
  putchar('S'); /* indicates beginning of a string */
  achar=0;
  while(achar != '"' && achar != EOF)
  {
    achar=fgetc(infile);
    if(achar != 0) /* null terminators in text form, lol */
    {
      if(achar == '"')
      {
        /* end string */
        printf("0"); /* tack on the terminator*/
      }
      else
      {
        printf("%d,",achar);
      }
    }
  }
}


int main(int argc, char **argv)
{
  if(argc == 2)
  {
    FILE *infile=fopen(argv[1], "rb");
    if(infile == NULL)
    {
      printf("Error: failed to open file.\n");
      return -1;
    }
    int achar=0;
    int i=0;
    /* copy header verbatim */
    /* (two lines) */
    while(i<2)
    {
      achar=fgetc(infile);
      if(achar == EOF || achar == 0)
      {
        fprintf(stderr, "ascii2csv: E: Unexpected/early EOF in file %s\n",argv[1]);
        return -1;
      }
      else
      {
        if(achar == '\n')
        {
          i++;
        }
        putchar((char)achar);
      }
    }

    /* translate back into stupid integer format */
    while(achar != EOF)
    {
      achar=fgetc(infile);
      if(achar != EOF)
      {
        if(achar == '"')
        {
          strdec(achar, infile);
        }
        else
        {
          printf("%c",achar);
        }
      }
    }
    fclose(infile);
  }
  else
  {
    fprintf(stderr, "Error: I need to be given exactly one argument, a filename.\n");
  }
}

