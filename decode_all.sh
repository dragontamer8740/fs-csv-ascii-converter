#! /bin/sh
# readlink isn't a standard tool unfortunately
# this just makes sure csv2ascii is in the PATH, though. Edit as necessary.
PATH="$PATH"':'"$(dirname "$(readlink -f "$0")")"
mkdir -p editing
for file in FS*Save; do
  csv2ascii "$file" > 'editing/'"$file"
done
