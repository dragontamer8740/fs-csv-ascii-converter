Release binaries for amd64 Linux, i386 and amd64 OS X, and i386 Windows can be
downloaded from
[BinTray](https://bintray.com/dragontamer8740/fs-csv-ascii-converter/fs-tools/0.1#files).

C programs that convert Flexible Survival's newer 'export progress'
data format to human readable text and back again, since they changed from the
previous 'saveword' format recently. Uses only `putchar()`, `getchar()`, and
one `ungetc()` for I/O, and uses no `malloc()`'s.

I wrote this program extremely quickly and somewhat badly, so that's not really
too surprising. As far as C goes, this is most definitely not too good for
readability. It may have even been better had I written this in a different,
higher level language. But that's not what I did.

If you want to see what awful C text parsing looks like, look no further.
I used zero `malloc()`'s, but that shouldn't be an issue and at least it means
there shouldn't be any memory leaks.

Notes:
Each of the comma separated numbers is actually just a single character of
an ascii string, represented in decimal form for some reason.

Strings don't have to be null terminated in the text anymore. Sorry about that.

Some sample usage:

    $ ./csv2ascii FSPossessionSave > plaintext.txt
    
(edit the file `plaintext.txt`)

    $ ./ascii2csv plaintext.txt > FSPossessionSave
