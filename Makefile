CC=gcc
CFLAGS=-Wall

.PHONY: all clean

all: csv2ascii ascii2csv str2csv
	make $^

csv2ascii: csv2ascii.c
	$(CC) $(CFLAGS) -o $@ $^

ascii2csv: ascii2csv.c
	$(CC) $(CFLAGS) -o $@ $^

str2csv: str2csv.c
	$(CC) $(CFLAGS) -o $@ $^

clean:
	rm ascii2csv csv2ascii str2csv
