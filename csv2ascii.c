#include <stdio.h> /* printf */
#include <stdlib.h> /* atoi */
#include <ctype.h> /* isdigit */

/* This code is unfortunately pretty ugly, as a result of the expanding
   scope of the tool.
   It was originally intended merely for converting inventory files,
   which are far more predictable than some of the other kinds of save
   files in terms of things like where strings occur.

   As a result, this code works, but it's pretty terrible at the same time,
   and I wouldn't recommend using it for anything but inspiration to make
   something less terrible.
*/
int main(int argc, char **argv)
{
  if(argc == 2)
  {
    FILE *infile=fopen(argv[1], "rb");
    if(infile == NULL)
    {
      printf("Error: Failed to open file.\n");
      return -1;
    }
    int achar=0;
    int i=0;
    /* copy header verbatim */
    while(i<2)
    {
      achar=fgetc(infile);
      printf("%c",(char)achar);
      if(achar == '\n')
      {
        i++;
      }
    }
    /* now we have been carried to the first line we care about.*/
    /* each line starts with a letter S so we need to skip that */
    char str[4]; /* ascii is limited in size */
    str[3]='\0';
    i=0;
/*    if(fgetc(infile) == 'S') *//* progress ahead of 'S' */
    while(achar != EOF)
    {
      while(achar != ';' && achar != EOF )
      {
        if(achar == '\n')
        {
          /* we know the first bit of data on each line is a string key */
          /* since my current idea is to mark string beginnings with '(',
             do that as long as next char isn't EOF. Then come back.*/
          int tmp=fgetc(infile);
          if(tmp != EOF)
          {
            switch(tmp)
            {
            case 'S':
              putchar('"');
              ungetc(tmp,infile);
              break;
            default:
              while(tmp != EOF && tmp != 'S')
              {
                putchar(tmp);
                tmp=fgetc(infile);
              }
              if(tmp == 'S')
              {
                putchar('"');
              }
              ungetc(tmp,infile);
            }
          }
        }
        while(isdigit(achar=fgetc(infile)) && i < 3)
        {
          str[i]=achar;
          i++;
        }
        int isok=0;
        i=0;
        while(i<3 && str[i] != '\0' && isok == 0)
        {
          if(!isascii(str[i]) || str[i] == '\0')
          {
            isok=1;
          }
          i++;
        }
        if(isok==0 && atoi(str) != '\0')
        {
          printf("%c",(char)atoi(str));
        }
        str[0]='\0';
        str[1]='\0';
        str[2]='\0';
        i=0;
          
      } /* ';' reached */
      if(achar == ';')
      {
        /* terminate string */
/*        putchar('\0');*/
/*        putchar(')');*/
        putchar('"');
        putchar(';');
      }
      while(achar != '\n' && achar != EOF && achar != 'S' && isascii(achar))
      {
        achar=fgetc(infile);
        if(achar != '\0' && achar != 'S')
        {
          printf("%c",achar);
        }
        else if(achar == 'S')
        {
          putchar('"'); /* open new string */
        }
        
      } /* != EOF */
    }
    fclose(infile);
    return 0;
  }
  else
  {
    printf("Error: I need to be given exactly one argument, a filename.\n");
  }
}
